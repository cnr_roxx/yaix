# YAIX
## Yet Another Invoice fX

---
**Warning!**

YAIX project is 'work in progress' at the moment so none of its features is completed. You've been warned.

It's also the first serious attempt to wrap my mind around JavaFX. So don't be mad if application has obvious errors and generally does not work as expected.

---

YAIX is JavaFX application. It's meant to be help with generating and printing VAT invoice.

YAIX supports only Polish tax and invoice regulations at the moment.

## TODO

This code lacks of

1. JAR builds configuration
2. PDF generation
3. Unit tests
4. Sane UI
    1. Tabs(?) for different parts of invoice (Seller data, client, items)
5. Setting the currency of invoice
    1. Store in XML/SQL
    2. Select in UI
6. Localization of UI
    1. Select in UI
    2. Number format
    3. Translation
7. Seller data management
8. Client data management
9. SQL as data store