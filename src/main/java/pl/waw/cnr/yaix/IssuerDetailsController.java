package pl.waw.cnr.yaix;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import pl.waw.cnr.yaix.model.InvoiceModel;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class IssuerDetailsController implements Initializable {

    private static final Logger logger = Logger.getLogger(IssuerDetailsController.class.getName());

    private final InvoiceModel invoice = InvoiceModel.getInstance();

    @FXML
    private TextField fIssuerCompany;
    @FXML
    private TextField fIssuerName;
    @FXML
    private TextField fIssuerAddressCity;
    @FXML
    private TextField fIssuerAddressStreet;
    @FXML
    private TextField fIssuerTIN;
    @FXML
    private TextField fIssuerAccountNumber;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fIssuerCompany.textProperty().bindBidirectional(invoice.issuerCompanyProperty());
        fIssuerName.textProperty().bindBidirectional(invoice.issuerNameProperty());
        fIssuerAddressCity.textProperty().bindBidirectional(invoice.issuerAddressCityProperty());
        fIssuerAddressStreet.textProperty().bindBidirectional(invoice.issuerAddressStreetProperty());
        fIssuerTIN.textProperty().bindBidirectional(invoice.getIssuerTINProperty());
        fIssuerAccountNumber.textProperty().bindBidirectional(invoice.getIssuerAccountNumberProperty());
    }

    public void handleKeyInput(KeyEvent keyEvent) {
    }
}
