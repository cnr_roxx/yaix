package pl.waw.cnr.yaix.model;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ItemModel {

    /*
     * Defined values
     */
    private final SimpleStringProperty name = new SimpleStringProperty("");
    //    private final SimpleStringProperty quantity = new SimpleStringProperty("");
    private final SimpleObjectProperty<BigDecimal> quantity = new SimpleObjectProperty<>();
    private final SimpleStringProperty unitOfMeasurment = new SimpleStringProperty("");
    private final SimpleObjectProperty<BigDecimal> priceNettoPerUnit = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<BigDecimal> vatRate = new SimpleObjectProperty<>();

    /*
     * Computed values
     */
    private final SimpleObjectProperty<BigDecimal> netValue = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<BigDecimal> vatAmount = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<BigDecimal> grossValue = new SimpleObjectProperty<>();


    public ItemModel(String name, BigDecimal quantity, String unitOfMeasurment, BigDecimal priceNettoPerUnit,
                     BigDecimal vatRate) {
        this.name.set(name);
        this.quantity.set(quantity);
        this.unitOfMeasurment.set(unitOfMeasurment);
        this.priceNettoPerUnit.set(priceNettoPerUnit);
        this.vatRate.set(vatRate);
        computeNetValue();
        computeVatAmount();
        computeGrossValue();
    }

    // Name
    public String getName() {
        return name.get();
    }
    public SimpleStringProperty nameProperty() {
        return name;
    }
    public void setName(String name) {
        this.name.set(name);
    }

    // Quantity
    public BigDecimal getQuantity() {
        return quantity.get();
    }
    public SimpleStringProperty quantitytProperty() {
        return name;
    }
    public void setQuantity(BigDecimal quantity) {
        this.quantity.set(quantity);
    }

    // Unit of Measurment
    public String getUnitOfMeasurment() {
        return unitOfMeasurment.get();
    }
    public SimpleStringProperty unitOfMeasurmentProperty() {
        return unitOfMeasurment;
    }
    public void setUnitOfMeasurment(String unitOfMeasurment) {
        this.unitOfMeasurment.set(unitOfMeasurment);
    }

    // Price Netto per Unit
    public BigDecimal getPriceNettoPerUnit() {
        return priceNettoPerUnit.get();
    }
    public SimpleObjectProperty<BigDecimal> priceNettoPerUnitProperty() {
        return priceNettoPerUnit;
    }
    public void setPriceNettoPerUnit(BigDecimal priceNettoPerUnit) {
        this.priceNettoPerUnit.set(priceNettoPerUnit);
    }

    // Net Value
    public BigDecimal getNetValue() {
        return netValue.get();
    }
    public SimpleObjectProperty<BigDecimal> netValueProperty() {
        return netValue;
    }
    public void setNetValue(BigDecimal netValue) {
        this.netValue.set(netValue);
    }

    // Vat Rate
    public BigDecimal getVatRate() {
        return vatRate.get();
    }
    public SimpleObjectProperty<BigDecimal> vatRateProperty() {
        return vatRate;
    }
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate.set(vatRate);
    }// Vat Rate

    // Vat Amount
    public BigDecimal getVatAmount() {
        return vatAmount.get();
    }
    public SimpleObjectProperty<BigDecimal> vatAmountProperty() {
        return vatAmount;
    }
    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount.set(vatAmount);
    }

    // Gross Value
    public BigDecimal getGrossValue() {
        return grossValue.get();
    }
    public SimpleObjectProperty<BigDecimal> grossValueProperty() {
        return grossValue;
    }
    public void setGrossValue(BigDecimal grossValue) {
        this.grossValue.set(grossValue);
    }


    private void computeNetValue() {
        this.setNetValue(
                this.getQuantity().multiply(
                        this.getPriceNettoPerUnit()).setScale(2, RoundingMode.CEILING));
    }

    private void computeVatAmount() {
        this.setVatAmount(
                this.getNetValue().multiply(
                        this.getVatRate().divide(new BigDecimal(100))).setScale(2, RoundingMode.CEILING));
    }

    private void computeGrossValue() {
        this.setGrossValue(
                this.getNetValue().add(
                        this.getVatAmount()).setScale(2, RoundingMode.CEILING));
    }
}
