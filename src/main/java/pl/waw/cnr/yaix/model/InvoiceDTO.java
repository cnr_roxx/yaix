package pl.waw.cnr.yaix.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InvoiceDTO implements Serializable {

    private String invoiceDTOVersion = "";
    private String id = "";
    private LocalDate issueDate = LocalDate.now();
    private LocalDate sellDate = LocalDate.now();
    private String issuePlace = "";
    private String duePeriod = "";
    private String clientName = "";
    private String clientAddressCity = "";
    private String clientAddressStreet = "";
    private String clientTIN = "";
    private List<ItemDTO> itemList = new ArrayList<>();

    public String getInvoiceDTOVersion() {
        return invoiceDTOVersion;
    }

    public void setInvoiceDTOVersion(String invoiceDTOVersion) {
        this.invoiceDTOVersion = invoiceDTOVersion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getSellDate() {
        return sellDate;
    }

    public void setSellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
    }

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public String getDuePeriod() {
        return duePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        this.duePeriod = duePeriod;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddressCity() {
        return clientAddressCity;
    }

    public void setClientAddressCity(String clientAddressCity) {
        this.clientAddressCity = clientAddressCity;
    }

    public String getClientAddressStreet() {
        return clientAddressStreet;
    }

    public void setClientAddressStreet(String clientAddressStreet) {
        this.clientAddressStreet = clientAddressStreet;
    }

    public String getClientTIN() {
        return clientTIN;
    }

    public void setClientTIN(String clientTIN) {
        this.clientTIN = clientTIN;
    }

    public List<ItemDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemDTO> itemList) {
        this.itemList = itemList;
    }
}
