package pl.waw.cnr.yaix.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class InvoiceModel implements Serializable {

    private static InvoiceModel instance = null;

    protected InvoiceModel() {
        // Exists only to defeat instantiation.
    }

    public static InvoiceModel getInstance() {
        if(instance == null) {
            instance = new InvoiceModel();
            instance.reset();
        }
        return instance;
    }

    /*
     * Defined values
     */
    private final StringProperty id = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> issueDate = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> sellDate = new SimpleObjectProperty();
    private final StringProperty issuePlace = new SimpleStringProperty();
    private final StringProperty duePeriod = new SimpleStringProperty();
    private final StringProperty clientName = new SimpleStringProperty();
    private final StringProperty clientAddressCity = new SimpleStringProperty();
    private final StringProperty clientAddressStreet = new SimpleStringProperty();
    private final StringProperty clientTIN = new SimpleStringProperty();

    private final StringProperty issuerCompany = new SimpleStringProperty();
    private final StringProperty issuerName = new SimpleStringProperty();
    private final StringProperty issuerAddressCity = new SimpleStringProperty();
    private final StringProperty issuerAddressStreet = new SimpleStringProperty();
    private final StringProperty issuerTIN = new SimpleStringProperty();
    private final StringProperty issuerAccountNumber = new SimpleStringProperty();

    private final ObservableList<ItemModel> itemList = FXCollections.observableArrayList();



    public void reset() {
        instance.setId("");
        instance.setIssueDate(LocalDate.now());
        instance.setSellDate(LocalDate.now());
        instance.setIssuePlace("");
        instance.setDuePeriod("");
        instance.setClientName("");
        instance.setClientAddressCity("");
        instance.setClientAddressStreet("");
        instance.setClientTIN("");
        instance.setIssuerName("");
        instance.setIssuerCompany("");
        instance.setIssuerAddressCity("");
        instance.setIssuerAddressStreet("");
        instance.setIssuerTIN("");
        instance.setIssuerAccountNumber("");
        instance.getItemList().clear();

    }

    /*
        --- ID ---
     */
    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getId() {
        return id.get();
    }

    /*
        --- Issue date ---
     */
    public ObjectProperty<LocalDate> issueDateProperty() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate.set(issueDate);
    }

    public LocalDate getIssueDate() {
        return issueDate.get();
    }

    /*
        --- Sell date ---
     */
    public ObjectProperty<LocalDate> sellDateProperty() {
        return sellDate;
    }

    public void setSellDate(LocalDate sellDate) {
        this.sellDate.set(sellDate);
    }

    public LocalDate getSellDate() {
        return sellDate.get();
    }

    /*
        --- Issue place ---
     */
    public StringProperty issuePlaceProperty() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace.set(issuePlace);
    }

    public String getIssuePlace() {
        return issuePlace.get();
    }

    /*
        --- Due period ---
    */
    public StringProperty duePeriodProperty() {
        return duePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        this.duePeriod.set(duePeriod);
    }

    public String getDuePeriod() {
        return duePeriod.get();
    }


    /*
    --- Client name ---
    */
    public StringProperty clientNameProperty() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName.set(clientName);
    }

    public String getClientName() {
        return clientName.get();
    }

    /*
    --- Client address city ---
    */
    public StringProperty clientAddressCityProperty() {
        return clientAddressCity;
    }

    public void setClientAddressCity(String clientAddressCity) {
        this.clientAddressCity.set(clientAddressCity);
    }

    public String getClientAddressCity() {
        return clientAddressCity.get();
    }

    /*
    --- Client address street ---
    */
    public StringProperty clientAddressStreetProperty() {
        return clientAddressStreet;
    }

    public void setClientAddressStreet(String clientAddressStreet) {
        this.clientAddressStreet.set(clientAddressStreet);
    }

    public String getClientAddressStreet() {
        return clientAddressStreet.get();
    }


    /*
    --- Client Taxpayer Identification Number  ---
    */
    public StringProperty clientTINProperty() {
        return clientTIN;
    }

    public void setClientTIN(String clientTIN) {
        this.clientTIN.set(clientTIN);
    }

    public String getClientTIN() {
        return clientTIN.get();
    }

    /*
    --- Issuer company ---
    */
    public StringProperty issuerCompanyProperty() {
        return issuerCompany;
    }

    public void setIssuerCompany(String issuerCompany) {
        this.issuerCompany.set(issuerCompany);
    }

    public String getIssuerCompany() {
        return issuerCompany.get();
    }

    /*
    --- Issuer name ---
    */
    public StringProperty issuerNameProperty() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName.set(issuerName);
    }

    public String getIssuerName() {
        return issuerName.get();
    }


    /*
    --- Issuer address city ---
    */
    public StringProperty issuerAddressCityProperty() {
        return issuerAddressCity;
    }

    public void setIssuerAddressCity(String issuerAddressCity) {
        this.issuerAddressCity.set(issuerAddressCity);
    }

    public String getIssuerAddressCity() {
        return issuerAddressCity.get();
    }

    /*
    --- Issuer address street ---
    */
    public StringProperty issuerAddressStreetProperty() {
        return issuerAddressStreet;
    }

    public void setIssuerAddressStreet(String issuerAddressStreet) {
        this.issuerAddressStreet.set(issuerAddressStreet);
    }

    public String getIssuerAddressStreet() {
        return issuerAddressStreet.get();
    }

    /*
    --- Issuer Taxpayer Identification Number (NIP) ---
    */
    public StringProperty getIssuerTINProperty() {
        return issuerTIN;
    }

    public void setIssuerTIN(String issuerTIN) {
        this.issuerTIN.set(issuerTIN);
    }

    public String getIssuerTIN() {
        return issuerTIN.get();
    }

    /*
    --- Issuer Account Number ---
    */
    public StringProperty getIssuerAccountNumberProperty() {
        return issuerAccountNumber;
    }

    public void setIssuerAccountNumber(String issuerAccountNumber) {
        this.issuerAccountNumber.set(issuerAccountNumber);
    }

    public String getIssuerAccountNumber() {
        return issuerAccountNumber.get();
    }

    /*
    --- Item list ---
    */
    public ObservableList<ItemModel> getItemList() {
        return itemList;
    }

//    public void setItemList(ObservableList<ItemModel> itemList) {
//        this.itemList = itemList;
//    }

    public BigDecimal countTotalVat() {
        BigDecimal total = new BigDecimal(0);
        for (ItemModel item : instance.getItemList()) {
            total = total.add(item.getVatAmount());
        }
        return total;
    }

    public BigDecimal countTotalGrossValue() {
        BigDecimal total = new BigDecimal(0);
        for (ItemModel item : instance.getItemList()) {
            total = total.add(item.getGrossValue());
        }
        return total;
    }

}
