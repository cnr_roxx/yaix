package pl.waw.cnr.yaix.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class ItemDTO implements Serializable {

    /*
     * Defined values
     */
    private String name = "";
    private BigDecimal quantity = null;
    private String unitOfMeasurment = "";
    private BigDecimal priceNettoPerUnit = null;
    private BigDecimal netValue = null;
    private BigDecimal vatRate = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getUnitOfMeasurment() {
        return unitOfMeasurment;
    }

    public void setUnitOfMeasurment(String unitOfMeasurment) {
        this.unitOfMeasurment = unitOfMeasurment;
    }

    public BigDecimal getPriceNettoPerUnit() {
        return priceNettoPerUnit;
    }

    public void setPriceNettoPerUnit(BigDecimal priceNettoPerUnit) {
        this.priceNettoPerUnit = priceNettoPerUnit;
    }

    public BigDecimal getNetValue() {
        return netValue;
    }

    public void setNetValue(BigDecimal netValue) {
        this.netValue = netValue;
    }

    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

}
