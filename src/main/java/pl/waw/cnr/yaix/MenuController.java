package pl.waw.cnr.yaix;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.waw.cnr.yaix.model.InvoiceDTO;
import pl.waw.cnr.yaix.model.InvoiceModel;
import pl.waw.cnr.yaix.model.ItemDTO;
import pl.waw.cnr.yaix.model.ItemModel;

import java.io.*;
import java.math.RoundingMode;
import java.util.logging.Logger;

public class MenuController {

    private static final Logger logger = Logger.getLogger(MenuController.class.getName());
    private final InvoiceModel invoice = InvoiceModel.getInstance();
    private File currentFile = null;

    @FXML
    private MenuBar menuBar;

    public void handleKeyInput(KeyEvent keyEvent) {
    }

    @FXML
    protected void handleAboutAction(ActionEvent actionEvent) {
    }

    @FXML
    protected void exitAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void newAction(ActionEvent actionEvent) {
        currentFile = null;
        invoice.reset();
    }

    public void openIssuerDetailsAction(ActionEvent actionEvent) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("issuerDetails.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Dane wystawiającego");
            stage.setScene(new Scene(root, 550, 450));
            stage.show();
            // Hide this current window (if this is what you want)
//            ((Node)(actionEvent.getSource())).getScene().getWindow().hide();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void saveFileAction(ActionEvent actionEvent) {
        if (currentFile != null) {
            saveFile(currentFile);
        } else {
            saveAsFileAction(actionEvent);
        }
    }

    @FXML
    protected void saveAsFileAction(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Export as...");
        currentFile = chooser.showSaveDialog(menuBar.getScene().getWindow());

        saveFile(currentFile);
    }



    private InvoiceDTO modelToinvoiceDTO() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setInvoiceDTOVersion("2.0");
        invoiceDTO.setId(invoice.getId());
        invoiceDTO.setIssueDate(invoice.getIssueDate());
        invoiceDTO.setSellDate(invoice.getSellDate());
        invoiceDTO.setIssuePlace(invoice.getIssuePlace());
        invoiceDTO.setDuePeriod(invoice.getDuePeriod());
        invoiceDTO.setClientName(invoice.getClientName());
        invoiceDTO.setClientAddressCity(invoice.getClientAddressCity());
        invoiceDTO.setClientAddressStreet(invoice.getClientAddressStreet());
        invoiceDTO.setClientTIN(invoice.getClientTIN());

        ItemDTO itemDTO;
        for (ItemModel item : invoice.getItemList()) {
            itemDTO = new ItemDTO();
            itemDTO.setName(item.getName());
            itemDTO.setQuantity(item.getQuantity().setScale(2, RoundingMode.CEILING));
            itemDTO.setUnitOfMeasurment(item.getUnitOfMeasurment());
            itemDTO.setPriceNettoPerUnit(item.getPriceNettoPerUnit().setScale(2, RoundingMode.CEILING));
            itemDTO.setNetValue(item.getNetValue().setScale(2, RoundingMode.CEILING));
            itemDTO.setVatRate(item.getVatRate().setScale(0, RoundingMode.CEILING));

            invoiceDTO.getItemList().add(itemDTO);
        }
        return invoiceDTO;
    }

    private void saveFile(File file) {

        if (file != null) {
            try {
                FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                XStream xstream = new XStream(new StaxDriver());
                xstream.alias("invoice", InvoiceDTO.class);
                xstream.alias("item", ItemDTO.class);
                xstream.toXML(modelToinvoiceDTO(), osw);
                osw.close();
                fos.close();

            } catch (StreamException e) {
                logger.severe(String.format("Stream exception while parsing file: %s", file.getName()));
            } catch (UnsupportedEncodingException e) {
                logger.severe(String.format("Unsupported Encoding exception while openning file: %s", file.getName()));
            } catch (FileNotFoundException e) {
                logger.severe(String.format("File Not Found exception while openning file: %s", file.getName()));
            } catch (IOException e) {
                logger.severe(String.format("IO exception while openning file: %s", file.getName()));
            }
        }

    }

    public void openFileAction(ActionEvent actionEvent) throws FileNotFoundException {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Import file...");
        currentFile = chooser.showOpenDialog(menuBar.getScene().getWindow());

        if (currentFile != null) {

            InputStream targetStream = new FileInputStream(currentFile);

            XStream xstream = new XStream(new StaxDriver());
            xstream.alias("invoice", InvoiceDTO.class);
            xstream.alias("item", ItemDTO.class);
            try {
                Object fObject = xstream.fromXML(targetStream);
                InvoiceDTO invoiceDTO = (InvoiceDTO) fObject;

                invoiceDTOtoModel(invoiceDTO);

            } catch (StreamException e) {
                logger.severe(String.format("Error while parsing file: %s", currentFile.getName()));
            }

        }
    }

    private void invoiceDTOtoModel(InvoiceDTO invoiceDTO) {
        invoice.reset();

        invoice.setId(invoiceDTO.getId());
        invoice.setIssueDate(invoiceDTO.getIssueDate());
        invoice.setSellDate(invoiceDTO.getSellDate());
        invoice.setIssuePlace(invoiceDTO.getIssuePlace());
        invoice.setDuePeriod(invoiceDTO.getDuePeriod());
        invoice.setClientName(invoiceDTO.getClientName());
        invoice.setClientAddressCity(invoiceDTO.getClientAddressCity());
        invoice.setClientAddressStreet(invoiceDTO.getClientAddressStreet());
        invoice.setClientTIN(invoiceDTO.getClientTIN());

        ItemModel item;
        for (ItemDTO itemDTO : invoiceDTO.getItemList()) {
            item = new ItemModel(itemDTO.getName(),itemDTO.getQuantity(), itemDTO.getUnitOfMeasurment(),
                    itemDTO.getPriceNettoPerUnit(), itemDTO.getVatRate());

            invoice.getItemList().add(item);
        }

    }
}
