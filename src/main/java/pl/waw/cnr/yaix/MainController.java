package pl.waw.cnr.yaix;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.util.converter.BigDecimalStringConverter;
import pl.waw.cnr.yaix.constatns.VatRate;
import pl.waw.cnr.yaix.helpers.BigDecimalParser;
import pl.waw.cnr.yaix.helpers.DateConverter;
import pl.waw.cnr.yaix.model.InvoiceModel;
import pl.waw.cnr.yaix.model.ItemModel;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class MainController implements Initializable {

    private static final Logger logger = Logger.getLogger(MainController.class.getName());

    private final InvoiceModel invoice = InvoiceModel.getInstance();
    private final DateConverter<LocalDate> dateConverter = new DateConverter<>();


    /*
     * Invoice form
     */
    @FXML
    private TextField fNumber;
    @FXML
    private DatePicker fIssueDate;
    @FXML
    private DatePicker fSellDate;
    @FXML
    private TextField fIssuePlace;
    @FXML
    private TextField fDuePeriod;
    @FXML
    private TextField fClientName;
    @FXML
    private TextField fClientAddressCity;
    @FXML
    private TextField fClientAddressStreet;
    @FXML
    private TextField fClientTIN;
    @FXML
    private TextField fIssuerCompany;
    @FXML
    private TextField fIssuerName;
    @FXML
    private TextField fIssuerAddressCity;
    @FXML
    private TextField fIssuerAddressStreet;
    @FXML
    private TextField fIssuerTIN;
    @FXML
    private TextField fIssuerAccountNumber;

    /*
     * Items table
     */
    @FXML
    private TableView<ItemModel> fItemList;
    @FXML
    private TableColumn<ItemModel, String> iNameColumn;
    @FXML
    private TableColumn<ItemModel, String> iUnitOfMeasurmentColumn;
    @FXML
    private ChoiceBox iVatRate;


    /*
     * New item form
     */
    @FXML
    private TextField iName;
    @FXML
    private TextField iQuantity;
    @FXML
    private TextField iUnitOfMeasurment;
    @FXML
    private TextField iPriceNettoPerUnit;


    /*
     *  Total values
     */
    @FXML
    private Text fInvoiceTotalGrossValue;
    @FXML
    private Text fInvoiceTotalVatAmount;

    public void initialize(URL location, ResourceBundle resources) {

        fNumber.textProperty().bindBidirectional(invoice.idProperty());
        fIssueDate.setConverter(dateConverter);
        fIssueDate.valueProperty().bindBidirectional(invoice.issueDateProperty());
        fSellDate.setConverter(dateConverter);
        fSellDate.valueProperty().bindBidirectional(invoice.sellDateProperty());
        fIssuePlace.textProperty().bindBidirectional(invoice.issuePlaceProperty());
        fDuePeriod.textProperty().bindBidirectional(invoice.duePeriodProperty());

        fClientName.textProperty().bindBidirectional(invoice.clientNameProperty());
        fClientAddressCity.textProperty().bindBidirectional(invoice.clientAddressCityProperty());
        fClientAddressStreet.textProperty().bindBidirectional(invoice.clientAddressStreetProperty());
        fClientTIN.textProperty().bindBidirectional(invoice.clientTINProperty());

        fIssuerCompany.textProperty().bindBidirectional(invoice.issuerCompanyProperty());
        fIssuerName.textProperty().bindBidirectional(invoice.issuerNameProperty());
        fIssuerAddressCity.textProperty().bindBidirectional(invoice.issuerAddressCityProperty());
        fIssuerAddressStreet.textProperty().bindBidirectional(invoice.issuerAddressStreetProperty());
        fIssuerTIN.textProperty().bindBidirectional(invoice.getIssuerTINProperty());
        fIssuerAccountNumber.textProperty().bindBidirectional(invoice.getIssuerAccountNumberProperty());

        fItemList.setItems(invoice.getItemList());

        iVatRate.getItems().add(VatRate.VAT_23);
        iVatRate.getItems().add(VatRate.VAT_8);
        iVatRate.getItems().add(VatRate.VAT_5);
        iVatRate.getItems().add(VatRate.VAT_0);

//        setupNameColumn();
        setTableEditable();

//        fInvoiceTotalGrossValue.textProperty().bindBidirectional(invoice.getInvoiceTotalGrossValueProperty(),
//                new BigDecimalStringConverter());

    }

    public void handleAboutAction(ActionEvent actionEvent) {
    }

    public void handleKeyInput(KeyEvent keyEvent) {
    }

    @FXML
    protected void addItem(ActionEvent actionEvent) {

        cleanupDataInItemForm();

        if (validateItemForm().equals(Boolean.FALSE))
            return;
        MathContext m = new MathContext(2);
        BigDecimal quantity = BigDecimalParser.parseFromString(iQuantity.getText())
                .setScale(2, RoundingMode.CEILING);
        BigDecimal priceNettoPerUnit = BigDecimalParser.parseFromString(iPriceNettoPerUnit.getText())
                .setScale(2, RoundingMode.CEILING);
        BigDecimal vatRate = (BigDecimal) iVatRate.getValue();
        vatRate.setScale(0, RoundingMode.CEILING);

        ItemModel newItem = new ItemModel(iName.getText(), quantity, iUnitOfMeasurment.getText(),
                priceNettoPerUnit, vatRate);

        invoice.getItemList().add(newItem);

        int i = invoice.getItemList().indexOf(newItem);

        logger.info(String.format("Name: '%s'; Quantity: '%s'; UnitOfMeasurment: '%s'; VatRate: '%s'; VatAmount: '%s'",
                invoice.getItemList().get(i).getName(), invoice.getItemList().get(i).getQuantity(),
                invoice.getItemList().get(i).getUnitOfMeasurment(), invoice.getItemList().get(i).getVatRate(),
                invoice.getItemList().get(i).getVatAmount()
                ));

        // total value
        fInvoiceTotalVatAmount.setText(invoice.countTotalVat().toString());
        fInvoiceTotalGrossValue.setText(invoice.countTotalGrossValue().toString());

        clearItemForm();

    }

    /*
     * Trims strings typed in "Item form"
     */
    private void cleanupDataInItemForm() {
        iName.setText(iName.getText().trim());
        iQuantity.setText(iQuantity.getText().trim());
        iUnitOfMeasurment.setText(iUnitOfMeasurment.getText().trim());
        iPriceNettoPerUnit.setText(iPriceNettoPerUnit.getText().trim());
    }

    /*
     * Clears "Item form"
     */
    private void clearItemForm() {
        iName.clear();
        iQuantity.clear();
        iUnitOfMeasurment.clear();
        iPriceNettoPerUnit.clear();
    }

    private Boolean validateItemForm() {
        Boolean result = Boolean.FALSE;

        if (!iName.getText().equals(""))
            result = Boolean.TRUE;
        return result;
    }

//    private void setupNameColumn() {

//        iNameColumn.setCellFactory(
//                EditCell.<ItemModel, String>forTableColumn(new DefaultStringConverter())
//        );

//        iNameColumn.setOnEditCommit(event -> {
//            final String value = event.getNewValue() != null ? event.getNewValue() : event.getOldValue();
//            logger.info(String.format("Event: '%s'", value));
//            ((ItemModel) event.getTableView().getItems()
//                    .get(event.getTablePosition().getRow()))
//                    .setName(value);
//
//            fItemList.refresh();
//        });
//
//    }

    private void setTableEditable() {

        fItemList.setEditable(true);

//        // allows the individual cells to be selected
//        fItemList.getSelectionModel().cellSelectionEnabledProperty().set(true);
//
//        // when character or numbers pressed it will start edit in editable fields
//        fItemList.setOnKeyPressed(event -> {
//
//            if (event.getCode().isLetterKey() || event.getCode().isDigitKey()) {
//
//                editFocusedCell();
//            } else if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.TAB) {
//
//                fItemList.getSelectionModel().selectNext();
//                event.consume();
//            } else if (event.getCode() == KeyCode.LEFT) {
//
//                // work around due to
//                // TableView.getSelectionModel().selectPrevious() due to a bug
//                // stopping it from working on
//                // the first column in the last row of the table
//                selectPrevious();
//
//                event.consume();
//            }
//        });
    }

//    @SuppressWarnings("unchecked")
//    private void editFocusedCell() {
//        final TablePosition<ItemModel, ?> focusedCell = fItemList.focusModelProperty().get().focusedCellProperty().get();
//        logger.info(String.format("Focused cell: row: %d", focusedCell.getRow()));
//        fItemList.edit(focusedCell.getRow(), focusedCell.getTableColumn());
//    }

//    @SuppressWarnings("unchecked")
//    private void selectPrevious() {
//
//        if (fItemList.getSelectionModel().isCellSelectionEnabled()) {
//
//            // in cell selection mode, we have to wrap around, going from
//            // right-to-left, and then wrapping to the end of the previous line
//            TablePosition<ItemModel, ?> pos = fItemList.getFocusModel().getFocusedCell();
//
//            if (pos.getColumn() - 1 >= 0) {
//
//                // go to previous row
//                fItemList.getSelectionModel().select(pos.getRow(), getTableColumn(pos.getTableColumn(), -1));
//            } else if (pos.getRow() < fItemList.getItems().size()) {
//                // wrap to end of previous row
//                fItemList.getSelectionModel().select(pos.getRow() - 1,
//                        fItemList.getVisibleLeafColumn(
//                                fItemList.getVisibleLeafColumns().size() - 1));
//            }
//        } else {
//            int focusIndex = fItemList.getFocusModel().getFocusedIndex();
//            if (focusIndex == -1) {
//                fItemList.getSelectionModel().select(fItemList.getItems().size() - 1);
//            } else if (focusIndex > 0) {
//                fItemList.getSelectionModel().select(focusIndex - 1);
//            }
//        }
//    }

//    private TableColumn<ItemModel, ?> getTableColumn(final TableColumn<ItemModel, ?> column, int offset) {
//
//        int columnIndex = fItemList.getVisibleLeafIndex(column);
//        int newColumnIndex = columnIndex + offset;
//        return fItemList.getVisibleLeafColumn(newColumnIndex);
//    }

    public void removeItem(ActionEvent actionEvent) {
        ItemModel selected = fItemList.getSelectionModel().getSelectedItem();
        logger.info(String.format("Removing item: '%s'", selected.getName()));
        fItemList.getItems().remove(selected);

        fInvoiceTotalVatAmount.setText(invoice.countTotalVat().toString());
        fInvoiceTotalGrossValue.setText(invoice.countTotalGrossValue().toString());
    }

}
