package pl.waw.cnr.yaix.constatns;

import java.math.BigDecimal;

public class VatRate {
    public static BigDecimal VAT_0 = new BigDecimal(0);
    public static BigDecimal VAT_5 = new BigDecimal(5);
    public static BigDecimal VAT_8 = new BigDecimal(8);
    public static BigDecimal VAT_23 = new BigDecimal(23);;
}
