package pl.waw.cnr.yaix.helpers;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Locale;

public class BigDecimalParser {

    /*
     * Used only for converting strings typed in form to BigDecimal field of InvoiceModel class.
     * BigDecimals are stored in en-US format in XML files
     */
    public static BigDecimal parseFromString(String numberInString) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.forLanguageTag("en-US"));
        df.setParseBigDecimal(true);
        return (BigDecimal)df.parse(numberInString, new ParsePosition(0));
    }
}
